<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cl54-a-wordp-5k3');

/** MySQL database username */
define('DB_USER', 'cl54-a-wordp-5k3');

/** MySQL database password */
define('DB_PASSWORD', 'nWFV9U!-D');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'r2G8Wo7L6dgXFehRWu9mrh+Ixma/ecpZGBaS^41AyXXNq6ddld-DO9CHnEI=8hzD');
define('SECURE_AUTH_KEY',  'gnBNPJev00hgVAw6+CDdsrvIcmbL!eF!Q3dpu!d(cQyI0^A!RSqJOx_p^=0mG5ei');
define('LOGGED_IN_KEY',    '^16jcb2P(Et9ak#qh7P)6!uC3Lo-j6M5G7IVBMLoiy+9AKkaVPM!#Xy_RgqUZQQs');
define('NONCE_KEY',        '(_mEUhm6GTDMI6(nbF7GZ41yWSm0)rZJgShvVhvQOEdwJ(VuUvcuc1FjDaMoYfyc');
define('AUTH_SALT',        'Zk)9VmgVjCckCsCwV7_G/=yrVHiDyvQ-EFTHWFzq_dd2YfcfNFKl(MuqoZI0g1aO');
define('SECURE_AUTH_SALT', 'LEe3hFkWI79rWN/GWJM(tT)d9z+n#Y1Lhc9)3UH4f)QM8UVpCiC_Po!p0/dap64C');
define('LOGGED_IN_SALT',   '4/bdj9Pi_-Lzt(a2vjFeFm)hk0f-xaQX^afSagJD4m4/=+!u0MdzVjt+0tP2MH9C');
define('NONCE_SALT',       'k3X-KyagV5LO_+3gRv67Kxc9cz^y29M_cJ8TB6(/oQ#l3Q=jJQmY#x6pG+l+wrj4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de.mo to wp-content/languages and set WPLANG to 'de' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/**
 *  Change this to true to run multiple blogs on this installation.
 *  Then login as admin and go to Tools -> Network
 */
define('WP_ALLOW_MULTISITE', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/* Destination directory for file streaming */
define('WP_TEMP_DIR', ABSPATH . 'wp-content/');

