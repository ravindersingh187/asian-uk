<?php
/*
 * Copyright 2012-2014, Theia Post Slider, WeCodePixels, http://wecodepixels.com
 */

class TpsAjax
{
    public static function query_vars($vars)
    {
        $vars[] = 'theiaPostSlider';
        $vars[] = 'postId';
        $vars[] = 'slides';

        return $vars;
    }

    public static function parse_request($wp)
    {
        if (!array_key_exists('theiaPostSlider', $wp->query_vars)) {
            return;
        }

        switch ($wp->query_vars['theiaPostSlider']) {
            case 'get-slides':
                self::getSlides($wp);
                break;
        }
    }

    private static function getSlides($wp)
    {
        if (
            !array_key_exists('postId', $wp->query_vars) ||
            !array_key_exists('slides', $wp->query_vars)
        ) {
            return;
        }

        TpsMisc::$forceDisable = true;

        // Get post.
        global $post, $pages;
        $post = get_post($wp->query_vars['postId']);
        if ($post === null) {
            exit();
        }
        setup_postdata($post);
        query_posts('p=' . $wp->query_vars['postId']);

        // Get and process each slide.
        $requestedSlides = $wp->query_vars['slides'];
        $slides = array();
        foreach ($requestedSlides as $i) {
            $slides[$i] = TpsMisc::getSubPage($i + 1, null);
        }

        // Add previous and next slide permalinks.
        {
            $previous = min($requestedSlides) - 1;
            $url = TpsMisc::getPostPageUrl($previous + 1);
            if ($url) {
                $slides[$previous] = array(
                    'permalink' => $url
                );
            }

            $next = max($requestedSlides) + 1;
            $url = TpsMisc::getPostPageUrl($next + 1);
            if ($url) {
                $slides[$next] = array(
                    'permalink' => $url
                );
            }
        }

        $result = array(
            'postId' => $post->ID,
            'slides' => $slides
        );

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($result);

        exit();
    }
}
