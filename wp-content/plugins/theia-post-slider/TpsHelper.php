<?php
/*
 * Copyright 2012-2014, Theia Post Slider, WeCodePixels, http://wecodepixels.com
 */

class TpsHelper
{
    /**
     * Get the HTML for a navigation bar. Must be called AFTER/BELOW the post content.
     * @param string|array $classes Any additional CSS classes to be added.
     * @return string
     */
    public static function getNavigationBar($classes = array()) {
        global $post, $page, $pages;

        return TpsMisc::getNavigationBar(array(
            'currentSlide' => $page,
            'totalSlides' => count($pages),
            'prevPostUrl' => $post->theiaPostSlider['prevPostUrl'],
            'nextPostUrl' => $post->theiaPostSlider['nextPostUrl'],
            'class' => $classes,
            'title' => TpsMisc::$currentPostTitle
        ));
    }
}