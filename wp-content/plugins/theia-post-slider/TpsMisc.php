<?php
/*
 * Copyright 2012-2014, Theia Post Slider, WeCodePixels, http://wecodepixels.com
 */

class TpsMisc
{
    public static $beginComment = '<p><!-- BEGIN THEIA POST SLIDER --></p>';

    public static $endComment = '<p><!-- END THEIA POST SLIDER --></p>';

    public static $beginHeaderShortCode = '[tps_header]';

    public static $endHeaderShortCode = '[/tps_header]';

    public static $beginTitleShortCode = '[tps_title]';

    public static $endTitleShortCode = '[/tps_title]';

    public static $beginFooterShortCode = '[tps_footer]';

    public static $endFooterShortCode = '[/tps_footer]';

    // Set this to true to prevent the_content() from calling itself in an infinite loop.
    public static $theContentIsCalled = false;

    // The posts for which we have enabled the slider (i.e. the script has been appended).
    public static $postsWithSlider = array();

    // If true, the slider won't be added to posts.
    public static $forceDisable = false;

    // Forcefully add $beginComment and $endComment.
    public static $forceBeginAndEndComments = false;

    // The [tps_title] of the current post.
    public static $currentPostTitle = null;

    // The current post's previous post URL.
    public static $postPreviousPostUrl = null;

    // The current post's next post URL.
    public static $postNextPostUrl = null;

    public static function init()
    {
        // Decide whether to use these files as a standalone plugin or not (i.e. integrate them in a theme).
        if (!defined('TPS_USE_AS_STANDALONE')) {
            define('TPS_USE_AS_STANDALONE', true);
        }

        // Define the plugin URL.
        if (!defined('TPS_PLUGINS_URL')) {
            define('TPS_PLUGINS_URL', plugins_url('', __FILE__) . '/');
        }

        // Initialize plugin options.
        TpsOptions::initOptions();
    }

    // The first filter that is called for get_the_excerpt.
    public static function beforeGetTheExcerpt($excerpt)
    {
        self::$forceDisable = true;

        return $excerpt;
    }

    // The last filter that is called for get_the_excerpt.
    public static function afterGetTheExcerpt($excerpt)
    {
        self::$forceDisable = false;

        return $excerpt;
    }

    /*
     * We want to enable sliders only for the main post on a post page. This usually means that is_singular() returns true
     * (i.e. the query is for only one post). But, some themes have single queries used only to display the excerpts.
     * So, here we'll prepare the post for sliders, but these sliders will be activated only if the_content() is also
     * called.
     */
    public static function the_post($post)
    {
        if (
            self::$forceDisable ||
            !self::isCompatiblePost()
        ) {
            return;
        }

        global $post, $page, $pages, $multipage;

        // If a page does not exist, display the last page.
        if ($page > count($pages)) {
            $page = count($pages);
        }

        // Get previous and next posts.
        $prevPost = self::getPrevNextPost(true);
        $nextPost = self::getPrevNextPost(false);

        /*
         * Prepare the sliders if
         * a) This is a single post with multiple pages.
         * - OR -
         * b) Previous/next post navigation is enabled and we do have a previous or a next post.
         */
        if (!($multipage || $prevPost || $nextPost)) {
            return;
        }

        // Save some variables that we'll also use in the_content().
        $post->theiaPostSlider = array(
            'slideContainerId' => 'tps_slideContainer_' . $post->ID,
            'navIdUpper' => 'tps_nav_upper_' . $post->ID,
            'navIdLower' => 'tps_nav_lower_' . $post->ID,
            'prevPostId' => $prevPost,
            'nextPostId' => $nextPost,
            'prevPostUrl' => $prevPost ? get_permalink($prevPost) : null,
            'nextPostUrl' => $nextPost ? get_permalink($nextPost) : null
        );

        // Set this to false so that the theme doesn't display pagination buttons. Kind of a hack.
        $multipage = false;
    }

    public static function the_content_early($content)
    {
        if (!TpsMisc::$forceBeginAndEndComments && (
            self::$forceDisable ||
            !self::isCompatiblePost()
        )) {
            return $content;
        }

        // Add strings to delimit the content.
        $content = TpsMisc::$beginComment . "\n" . $content . "\n" . TpsMisc::$endComment;

        // Be sure that shortcodes are in their own paragraph.
        $shortcodes = array(
            TpsMisc::$beginHeaderShortCode,
            TpsMisc::$endHeaderShortCode,
            TpsMisc::$beginTitleShortCode,
            TpsMisc::$endTitleShortCode,
            TpsMisc::$beginFooterShortCode,
            TpsMisc::$endFooterShortCode
        );
        foreach ($shortcodes as $sc) {
            $content = str_replace($sc, "\n\n" . $sc . "\n\n", $content);
        }

        return $content;
    }

    /*
     * Append the JavaScript code only if the_content is called (i.e. the whole post is being displayed, not just the
     * excerpt).
     */
    public static function the_content($content)
    {
        global $post, $page, $pages;

        if (
            self::$forceDisable ||
            !self::isCompatiblePost() ||
            !isset($post) ||
            !property_exists($post, 'theiaPostSlider')
        ) {
            // Remove shortcodes.
            $content = str_replace(array(
                TpsMisc::$beginHeaderShortCode,
                TpsMisc::$endHeaderShortCode,
                TpsMisc::$beginTitleShortCode,
                TpsMisc::$endTitleShortCode,
                TpsMisc::$beginFooterShortCode,
                TpsMisc::$endFooterShortCode
            ), '', $content);

            return $content;
        }

        // Prevent this function from calling itself.
        if (self::$theContentIsCalled) {
            return $content;
        }
        self::$theContentIsCalled = true;

        $currentPage = min(max($page, 1), count($pages));

        // Extract short codes. This needs to be done before splitting the content.
        {
            self::$currentPostTitle = TpsShortCodes::extractShortCode($content, TpsMisc::$beginTitleShortCode, TpsMisc::$endTitleShortCode);

            if ($page == 1) { $contentToExtractHeaderFrom = &$content; } else { $contentToExtractHeaderFrom = &$pages[0]; }
            $header = TpsShortCodes::extractShortCode($contentToExtractHeaderFrom, TpsMisc::$beginHeaderShortCode, TpsMisc::$endHeaderShortCode);

            if ($page == count($pages)) { $contentToExtractFooterFrom = &$content; } else { $contentToExtractFooterFrom = &$pages[count($pages) - 1]; }
            $footer = TpsShortCodes::extractShortCode($contentToExtractFooterFrom, TpsMisc::$beginFooterShortCode, TpsMisc::$endFooterShortCode);
        }

        // Split the content.
        $splitContent = TpsMisc::splitContent($content);
        $content = $splitContent['content'];

        // Add slider HTML.
        {
            $html = $splitContent['beforeContent'];

            // Header
            $html .= '<div class="theiaPostSlider_header _header">' . $header . '</div>';

            // Top slider
            if (in_array(TpsOptions::get('nav_vertical_position'), array('top_and_bottom', 'top'))) {
                $html .= TpsMisc::getNavigationBar(array(
                    'currentSlide' => $page,
                    'totalSlides' => count($pages),
                    'prevPostUrl' => $post->theiaPostSlider['prevPostUrl'],
                    'nextPostUrl' => $post->theiaPostSlider['nextPostUrl'],
                    'id' => $post->theiaPostSlider['navIdUpper'],
                    'class' => '_upper',
                    'title' => self::$currentPostTitle
                ));
            }

            // Current slide.
            $html .= '<div id="' . $post->theiaPostSlider['slideContainerId'] . '" class="theiaPostSlider_slides"><div>';
            $html .= "\n\n" . $content . "\n\n";
            $html .= '</div></div>';

            // Bottom slider
            if (in_array(TpsOptions::get('nav_vertical_position'), array('top_and_bottom', 'bottom'))) {
                $html .= TpsMisc::getNavigationBar(array(
                    'currentSlide' => $page,
                    'totalSlides' => count($pages),
                    'prevPostUrl' => $post->theiaPostSlider['prevPostUrl'],
                    'nextPostUrl' => $post->theiaPostSlider['nextPostUrl'],
                    'id' => $post->theiaPostSlider['navIdLower'],
                    'class' => '_lower',
                    'title' => self::$currentPostTitle
                ));
            }

            // Footer
            $html .= '<div class="theiaPostSlider_footer _footer">' . $footer . '</div>';

            $html .= $splitContent['afterContent'];
        }

        $slides = array();
        // Preload slides.
        {
            $preloadBegin = $currentPage + 1;
            $preloadEnd = $currentPage;

            if (TpsOptions::get('slide_loading_mechanism', 'tps_advanced') == 'all') {
                $preloadBegin = 1;
                $preloadEnd = count($pages);
            }

            if (TpsOptions::get('ad_refreshing_mechanism', 'tps_advanced') == 'page') {
                // Avoid AJAX request if the page refreshes every single slide.
                if (TpsOptions::get('refresh_ads_every_n_slides', 'tps_advanced') == 1) {
                    $preloadBegin = $currentPage + 1;
                    $preloadEnd = $currentPage;
                } else {
                    $preloadBegin = max($currentPage - TpsOptions::get('refresh_ads_every_n_slides', 'tps_advanced'), $preloadBegin);
                    $preloadEnd = min($currentPage + TpsOptions::get('refresh_ads_every_n_slides', 'tps_advanced'), $preloadEnd);
                }
            }

	        // Validate values.
            $preloadBegin = max(1, $preloadBegin);
            $preloadEnd = min(count($pages), $preloadEnd);

            for ($i = $preloadBegin; $i <= $preloadEnd; $i++) {
                // If we don't need to pass the source, then don't get the current slide since it will be echoed as actual HTML.
                if (!TpsOptions::get('do_not_cache_rendered_html', 'tps_advanced') && $i == $currentPage) {
                    continue;
                }

                if (TpsOptions::get('ad_refreshing_mechanism', 'tps_advanced') == 'page') {
                    // Only get permalinks for the edge slides.
                    if (
                        $i == $currentPage - TpsOptions::get('refresh_ads_every_n_slides', 'tps_advanced') ||
                        $i == $currentPage + TpsOptions::get('refresh_ads_every_n_slides', 'tps_advanced')
                    ) {
                        $slides[$i - 1] = array(
                            'permalink' => TpsMisc::getPostPageUrl($i)
                        );

                        continue;
                    }
                }

                // Get the entire slide.
                $slides[$i - 1] = TpsMisc::getSubPage($i, $currentPage);
            }
        }

        // Append the slider initialization script to the "theiaPostSlider.js" script.
        if (
            TpsOptions::get('slide_loading_mechanism', 'tps_advanced') != 'refresh' &&
            !(TpsOptions::get('do_not_check_for_multiple_instances', 'tps_advanced') == false && in_array($post->ID, self::$postsWithSlider) == true)
        ) {
            $sliderOptions = array(
                'slideContainer' => '#' . $post->theiaPostSlider['slideContainerId'],
                'nav' => array('.theiaPostSlider_nav'),
                'navText' => TpsOptions::get('navigation_text'),
                'helperText' => TpsOptions::get('helper_text'),
                'defaultSlide' => $currentPage - 1,
                'transitionEffect' => TpsOptions::get('transition_effect'),
                'transitionSpeed' => (int) TpsOptions::get('transition_speed'),
                'keyboardShortcuts' => (self::isCompatiblePost() && !TpsOptions::get('disable_keyboard_shortcuts', 'tps_nav')) ? true : false,
                'numberOfSlides' => count($pages),
                'slides' => $slides,
                'useSlideSources' => TpsOptions::get('do_not_cache_rendered_html', 'tps_advanced'),
                'prevPost' => $post->theiaPostSlider['prevPostUrl'],
                'nextPost' => $post->theiaPostSlider['nextPostUrl'],
                'prevText' => TpsOptions::get('theme_type') == 'font' ? TpsOptions::getFontIcon('left') : TpsOptions::get('prev_text'),
                'nextText' => TpsOptions::get('theme_type') == 'font' ? TpsOptions::getFontIcon('right') : TpsOptions::get('next_text'),
                'buttonWidth' => TpsOptions::get('button_width'),
                'prevText_post' => TpsOptions::get('theme_type') == 'font' ? TpsOptions::getFontIcon('left') : TpsOptions::get('prev_text_post'),
                'nextText_post' => TpsOptions::get('theme_type') == 'font' ? TpsOptions::getFontIcon('right') : TpsOptions::get('next_text_post'),
                'buttonWidth_post' => TpsOptions::get('button_width_post'),
                'postUrl' => get_permalink($post->ID),
                'postId' => $post->ID,
                'refreshAds' => TpsOptions::get('refresh_ads', 'tps_advanced'),
                'refreshAdsEveryNSlides' => TpsOptions::get('refresh_ads_every_n_slides', 'tps_advanced'),
                'adRefreshingMechanism' => TpsOptions::get('ad_refreshing_mechanism', 'tps_advanced'),
                'siteUrl' => get_site_url()
            );

            $vars = "
                var tpsInstance;
                var tpsOptions = " . json_encode($sliderOptions) . ";
            ";

            // If there are multiple sliders on the page (i.e. the theme has compatibility issues), the plugin options must not get overwritten by each other.
            $numberOfSliders = count(self::$postsWithSlider);

            // Append script to content.
            $script = "
                <script type='text/javascript'>
                    " . ($numberOfSliders == 0 ? $vars : '')  . "
                    (function ($) {
                        $(document).ready(function () {
                            " . ($numberOfSliders != 0 ? $vars : '')  . "
                            tpsInstance = new tps.createSlideshow(tpsOptions);
                        });
                    }(jQuery));
                </script>
            ";
            $html .= $script;

            // Mark the post as having a slider.
            self::$postsWithSlider[] = $post->ID;
        }

        self::$theContentIsCalled = false;

        return $html;
    }

    public static function echoInlineStyles($force = false)
    {
        if (TpsOptions::get('theme_type') != 'font' && $force !== true) {
            return;
        }

        $color = TpsColors::rgbToHsl(TpsColors::hexToRgb(TpsOptions::get('theme_font_color')));
        $hoverColor = $disabledColor = $color;

        if ($color[2] >= 0.2 && $color[2] <= 0.8) {
            $disabledColor[1] = 0;
        } elseif ($color[2] < 0.2) {
            // A very dark color.
            $disabledColor[1] = 0;;
            $disabledColor[2] += 0.4;
        } else {
            // A very bright color.
            $disabledColor[1] = 0;
            $disabledColor[2] -= 0.1;
        }
        if ($color[2] <= 0.8) {
            $hoverColor[2] += 0.1 + (0.1 * ((0.8 - $hoverColor[2]) / 0.8));
        } else {
            $hoverColor[2] -= 0.1;
        }

        ?>
        <style>
            .theiaPostSlider_nav.fontTheme ._title {
                line-height: <?php echo TpsOptions::get('theme_font_size'); ?>px;
            }

            .theiaPostSlider_nav.fontTheme ._prev,
            .theiaPostSlider_nav.fontTheme ._next {
                font-size: <?php echo TpsOptions::get('theme_font_size'); ?>px;
                color: <?php echo TpsOptions::get('theme_font_color'); ?>;
                line-height: 0;
            }

            .theiaPostSlider_nav.fontTheme ._prev:hover,
            .theiaPostSlider_nav.fontTheme ._next:hover {
                color: <?php echo TpsColors::rgbToHex(TpsColors::hslToRgb($hoverColor)); ?>;
            }

            .theiaPostSlider_nav.fontTheme ._disabled {
                color: <?php echo TpsColors::rgbToHex(TpsColors::hslToRgb($disabledColor)); ?> !important;
            }
        </style>
        <?php
    }

    // Is this post a "post" or a "page" (i.e. should we display the slider)?
    public static function isCompatiblePost()
    {
        $value = (is_single() || is_page() || is_attachment()) && in_array(get_post_type(), TpsOptions::get('post_types'));
        if ($value == false) {
            return false;
        }

        global $post;
        if ($post) {
            $options = TpsOptions::getPostOptions($post->ID);
            if ($options['enabled'] == false) {
                return false;
            }
        }

        return true;
    }

    // Get HTML for a navigation bar.
    public static function getNavigationBar($options)
    {
        $defaults = array(
            'currentSlide' => null,
            'totalSlides' => null,
            'prevPostId' => null,
            'nextPostId' => null,
            'prevPostUrl' => null,
            'nextPostUrl' => null,
            'id' => null,
            'class' => array(),
            'style' => null,
            'title' => null
        );
        $options = array_merge($defaults, $options);
        if (!is_array($options['class'])) {
            $options['class'] = array($options['class']);
        }

        // Get text
        if ($options['totalSlides'] == 1) {
            $text = '';
        } else {
            $text = TpsOptions::get('navigation_text');
            $text = str_replace('%{currentSlide}', $options['currentSlide'], $text);
            $text = str_replace('%{totalSlides}', $options['totalSlides'], $text);
        }

        // Get button URLs
        $prev = TpsMisc::getNavigationBarButton($options, false);
        $next = TpsMisc::getNavigationBarButton($options, true);

        // Title
        if (!$options['title']) {
            $options['title'] = '<span class="_helper">' . TpsOptions::get('helper_text') . '</span>';
        }

        // Final HTML
        $class = array('theiaPostSlider_nav');
        $class[] = '_' . TpsOptions::get('nav_horizontal_position');
        if (TpsOptions::get('theme_type') == 'font') {
            $class[] = 'fontTheme';
        }
        foreach ($options['class'] as $c) {
            $class[] = $c;
        }

        $html =
            '<div' . ($options['id'] !== null ? ' id="' . $options['id'] . '"' : '') . ($options['style'] !== null ? ' style="' . $options['style'] . '"' : '') . ' class="' . implode($class, ' ') . '">' .
            '<div class="_buttons">' . $prev . '<span class="_text">' . $text . '</span>' . $next . '</div>' .
            '<div class="_title">' . $options['title'] . '</div>' .
            '</div>';

        return $html;
    }

    /*
     * Get a button for a navigation bar.
     * @param direction boolean False = prev; True = next;
     */
    public static function getNavigationBarButton($options, $direction)
    {
        $directionName = $direction ? 'next' : 'prev';
        $url = self::getPostPageUrl($options['currentSlide'] + ($direction ? 1 : -1));

        // Check if there isn't another page but there is another post.
        $urlIsAnotherPost = !$url && $options[$directionName . 'PostUrl'];
        if ($urlIsAnotherPost) {
            $url = $options[$directionName . 'PostUrl'];
        }

        switch (TpsOptions::get('theme_type')) {
            case 'font':
                $text = $directionName == 'next' ? TpsOptions::getFontIcon('right') : TpsOptions::getFontIcon('left');
                $width = 0;
                break;

            case 'classic':
                // If there isn't another page but there is another post.
                if ($urlIsAnotherPost) {
                    $text = TpsOptions::get($directionName . '_text_post');
                    $width = TpsOptions::get('button_width_post');
                } else {
                    $text = TpsOptions::get($directionName . '_text');
                    $width = TpsOptions::get('button_width');
                }
                break;

            default:
                return '';
        }

        $style = $width == 0 ? '' : 'style="width: ' . $width . 'px"';
        $htmlPart1 = '<span class="_1"></span><span class="_2" ' . $style . '>';
        $htmlPart2 = '</span><span class="_3"></span>';

        // HTML
        $html = $htmlPart1 . $text . $htmlPart2;
        if ($url) {
            $button = '<a href="' . $url . '" class="_' . $directionName . '">' . $html . '</a>';
        } else {
            $button = '<span class="_' . $directionName . ' _disabled">' . $html . '</span>';
        }

        return $button;
    }

    // Get the previous or next post.
    public static function getPrevNextPost($previous)
    {
        if (!TpsOptions::get('post_navigation')) {
            return null;
        }
        $post = get_adjacent_post(TpsOptions::get('post_navigation_same_category'), '', $previous);
        if (!$post) {
            return null;
        }

        return $post->ID;
    }

    public static function getSubPage($pageNumber, $currentPageNumber = null)
    {
        global $page, $pages;

        // Set new page number
        $page = $pageNumber;
        $slide = array();
        $slide['title'] = self::getPageTitle();
        $slide['permalink'] = self::getPostPageUrl($page);

        // Get content
        $slideContent = get_the_content();

        // Remove header shortcode.
        if ($pageNumber == 1) {
            TpsShortCodes::extractShortCode($slideContent, TpsMisc::$beginHeaderShortCode, TpsMisc::$endHeaderShortCode);
        }

        // Remove footer shortcode.
        if ($pageNumber == count($pages)) {
            TpsShortCodes::extractShortCode($slideContent, TpsMisc::$beginFooterShortCode, TpsMisc::$endFooterShortCode);
        }

        // Save the shortcode title, if present.
        $slide['shortCodeTitle'] = TpsShortCodes::extractShortCode($slideContent, TpsMisc::$beginTitleShortCode, TpsMisc::$endTitleShortCode);

        // Apply filters.
        TpsMisc::$forceBeginAndEndComments = true;
        $slideContent = apply_filters('the_content', $slideContent);
        TpsMisc::$forceBeginAndEndComments = false;
        $slideContent = str_replace(']]>', ']]&gt;', $slideContent);

        /*
         * Leave only the actual text. Aditional headers or footers will be discarded. Plugins like "video quicktags"
         * will be left intact, while plugins like "related posts thumbnails" and "better author bio" will be discarded.
         */
        $splitContent = TpsMisc::splitContent($slideContent);
        $slideContent = $splitContent['content'];

        $slide['content'] = $slideContent;

        // Set back page number.
        $page = $currentPageNumber;

        return $slide;
    }

    // Add the "next page" button to the post editor.
    public static function wysiwyg_editor($mce_buttons)
    {
        $pos = array_search('wp_more', $mce_buttons, true);
        if ($pos !== false) {
            $tmp_buttons = array_slice($mce_buttons, 0, $pos + 1);
            $tmp_buttons[] = 'wp_page';
            $mce_buttons = array_merge($tmp_buttons, array_slice($mce_buttons, $pos + 1));
        }

        return $mce_buttons;
    }

    // Get the URL of a post's page.
    public static function getPostPageUrl($i)
    {
        global $post, $wp_rewrite, $pages;
        if ($i < 1 || $i > count($pages)) {
            return null;
        }
        if (1 == $i) {
            $url = get_permalink();
        } else {
            if ( '' == get_option('permalink_structure') || in_array($post->post_status, array('draft', 'pending')) )
                $url = add_query_arg( 'page', $i, get_permalink() );
            elseif ( 'page' == get_option('show_on_front') && get_option('page_on_front') == $post->ID )
                $url = trailingslashit(get_permalink()) . user_trailingslashit("$wp_rewrite->pagination_base/" . $i, 'single_paged');
            else
                $url = trailingslashit(get_permalink()) . user_trailingslashit($i, 'single_paged');
        }

        return $url;
    }

    /**
     * Tries to get the correct title of the page. Very hackish, but there's no other way.
     * @return string
     */
    public static function getPageTitle()
    {
        // Set the current page of the WP query since it's used by SEO plugins.
        global $wp_query, $page;
        $oldPage = $wp_query->get('page');
        if ($page > 1) {
            $wp_query->set('page', $page);
        } else {
            $wp_query->set('page', null);
        }

        // Get the title.
        $title = self::getPageTitleHelper();
        $title = html_entity_decode($title, ENT_QUOTES, 'UTF-8');

        // Set back the current page.
        $wp_query->set('page', $oldPage);

        // Return the title.
        return $title;
    }

    private static function getPageTitleHelper()
    {
        // If the WordPress SEO plugin is active and compatible.
        global $wpseo_front;
        if (
            isset($wpseo_front) &&
            method_exists($wpseo_front, 'title')
        ) {
            return $wpseo_front->title('', false);
        }

        // If the SEO Ultimate plugin is active and compatible.
        global $seo_ultimate;
        if (
            isset($seo_ultimate) &&
            property_exists($seo_ultimate, 'modules') &&
            isset($seo_ultimate->modules['titles']) &&
            method_exists($seo_ultimate->modules['titles'], 'get_title')
        ) {
            @$title = $seo_ultimate->modules['titles']->get_title();

            return $title;
        }

        // If all else fails, return the standard WordPress title. Unfortunately, most theme hard-code their <title> tag.
        return wp_title('', false);
    }

    /*
     * Split a string using the TpsMisc::$beginComment and TpsMisc::$endComment tags.
     */
    private static function splitContent($content)
    {
        $begin = TpsMisc::mb_strpos($content, TpsMisc::$beginComment);
        $end = TpsMisc::mb_strpos($content, TpsMisc::$endComment);

        if ($begin !== false && $end !== false) {
            // Cut!
            $beforeContent = TpsMisc::mb_substr($content, 0, $begin);
            $afterContent = TpsMisc::mb_substr($content, $end);

	        $begin += TpsMisc::mb_strlen(TpsMisc::$beginComment);
            $content = TpsMisc::mb_substr($content, $begin, $end - $begin);
        } else {
            $beforeContent = '';
            $afterContent = '';
        }

        // Trim left and right whitespaces.
        $content = trim($content);

        return array(
            'beforeContent' => $beforeContent,
            'content' => $content,
            'afterContent' => $afterContent
        );
    }

    public static function multibyteFunctionsExist()
    {
        // Cache result.
        static $result;

        if ($result === null) {
            $result = function_exists('mb_substr') && function_exists('mb_strlen') && function_exists('mb_strpos');
        }

        return $result;
    }

    public static function mb_substr()
    {
        $args = func_get_args();
        if (self::multibyteFunctionsExist()) {
            return call_user_func_array('mb_substr', $args);
        }

        return call_user_func_array('substr', $args);
    }

    public static function mb_strlen()
    {
        $args = func_get_args();
        if (self::multibyteFunctionsExist()) {
            return call_user_func_array('mb_strlen', $args);
        }

        return call_user_func_array('strlen', $args);
    }

    public static function mb_strpos()
    {
        $args = func_get_args();
        if (self::multibyteFunctionsExist()) {
            return call_user_func_array('mb_strpos', $args);
        }

        return call_user_func_array('strpos', $args);
    }
}
