<?php
/*
 * Copyright 2012-2014, Theia Post Slider, WeCodePixels, http://wecodepixels.com
 */

class TpsOptions
{
    public static function addMetaBoxes()
    {
        add_meta_box(
            'tps_options', // id, used as the html id att
            __( 'Theia Post Slider' ), // meta box title
            'TpsOptions::addMetaBoxesCallback', // callback function, spits out the content
            null, // post type or page. This adds to posts only
            'side', // context, where on the screen
            'low' // priority, where should this go in the context
        );
    }

    public static function addMetaBoxesCallback($post)
    {
        $options = TpsOptions::getPostOptions($post->ID);

        ?>
        <p>
            <label>
                <input type='hidden' value='false' name='tps_options[enabled]'>
                <input type="checkbox" value="true" name="tps_options[enabled]" <?php echo $options['enabled'] == true ? 'checked' : ''; ?>>
                Enable on this post.
            </label>
        </p>
        <?php

        do_action('tps_add_meta_boxes_callback', $post);
    }

    public static function savePost($postId)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return false;
        if (!current_user_can('edit_page', $postId)) return false;
        if (empty($postId)) return false;

        $defaults = array(
            'enabled' => TpsOptions::get('default_activation_behavior', 'tps_advanced') == 0 ? false : true
        );
        $options = array_key_exists('tps_options', $_REQUEST) ? $_REQUEST['tps_options'] : array();
        foreach ($options as $optionKey => $option) {
            if (!array_key_exists($optionKey, $defaults)) {
                unset($options[$optionKey]);
            } else {
                // Sanitize.
                if (is_bool($defaults[$optionKey])) {
                    $options[$optionKey] = ($options[$optionKey] === true || $options[$optionKey] === 'true') ? true : false;
                }
            }
        }
        $options = array_merge($defaults, $options);

        update_post_meta($postId, 'tps_options', $options);
    }

    // Get all available transition effects.
    public static function getTransitionEffects()
    {
        $options = array(
            'none' => 'None',
            'simple' => 'Simple',
            'slide' => 'Slide',
            'fade' => 'Fade'
        );

        return $options;
    }

    // Get button horizontal positions.
    public static function getButtonHorizontalPositions()
    {
        $options = array(
            'left' => 'Left',
            'center' => 'Center',
            'right' => 'Right'
        );

        return $options;
    }

    // Get button vertical positions.
    public static function getButtonVerticalPositions()
    {
        $options = array(
            'top_and_bottom' => 'Top and bottom',
            'top' => 'Top',
            'bottom' => 'Bottom',
            'none' => 'None'
        );

        return $options;
    }

    // Get all available themes.
    public static function getThemes()
    {
        $themes = array();

        // Special files to ignore
        $ignore = array('admin.css');

        // Get themes corresponding to .css files.
        $dir = dirname(__FILE__) . '/css';
        if ($handle = opendir($dir)) {
            while (false !== ($entry = readdir($handle))) {
                if (in_array($entry, $ignore)) {
                    continue;
                }

                $file = $dir . '/' . $entry;
                if (!is_file($file)) {
                    continue;
                }

                // Beautify name
                $name = substr($entry, 0, -4); // Remove ".css"
                $name = str_replace('--', ', ', $name);
                $name = str_replace('-', ' ', $name);
                $name = ucwords($name);

                // Add theme
                $themes[$entry] = $name;
            }
            closedir($handle);
        }

        $themes['none'] = 'None';

        // Sort alphabetically
        asort($themes);

        return $themes;
    }

    /**
     * @param $direction 'left' or 'right'
     * @return string
     */
    public static function getFontIcon($direction)
    {
        $class = $direction == 'left' ? TpsOptions::get('theme_font_leftClass') : TpsOptions::get('theme_font_rightClass');

        return '<span aria-hidden="true" class="' . $class . '"></span>';
    }

    public static function get($optionId, $optionGroups = array('tps_general', 'tps_nav', 'tps_advanced'))
    {
        if (!is_array($optionGroups)) {
            $optionGroups = array($optionGroups);
        }

        foreach ($optionGroups as $groupId) {
            $options = get_option($groupId);

            if (!is_array($options)) {
                continue;
            }

            if (array_key_exists($optionId, $options)) {
                return $options[$optionId];
            }
        }

        return null;
    }

    // Initialize options
    public static function initOptions()
    {
        $defaults = array(
            'tps_general' => array(
                'transition_effect' => 'slide',
                'transition_speed' => 400,
                'theme_type' => 'font',
                'theme' => 'buttons-orange.css',
                'theme_font_name' => 'chevron-circle',
                'theme_font_leftClass' => 'tps-icon-chevron-circle-left',
                'theme_font_rightClass' => 'tps-icon-chevron-circle-right',
                'theme_font_color' => '#f08100',
                'theme_font_size' => 48
            ),
            'tps_nav' => array(
                'navigation_text' => '%{currentSlide} of %{totalSlides}',
                'helper_text' => 'Use your ← → (arrow) keys to browse',
                'prev_text' => 'Prev',
                'next_text' => 'Next',
                'button_width' => 0,
                'prev_text_post' => 'Prev post',
                'next_text_post' => 'Next post',
                'button_width_post' => 0,
                'post_navigation' => false,
                'post_navigation_same_category' => false,
                'nav_horizontal_position' => 'right',
                'nav_vertical_position' => 'top_and_bottom',
                'disable_keyboard_shortcuts' => false
            ),
            'tps_advanced' => array(
                'default_activation_behavior' => 1,
	            'post_types' => array('post', 'page'),
                'slide_loading_mechanism' => 'ajax',
                'refresh_ads' => false,
                'ad_refreshing_mechanism' => 'javascript',
                'refresh_ads_every_n_slides' => 1,
                'do_not_check_for_multiple_instances' => true,
                'do_not_cache_rendered_html' => true,
                'enable_touch_gestures' => false
            ),
            'tps_about' => array()
        );

        // Reset to defaults.
        $resetToDefaults = get_option('tps_about');
        $resetToDefaults = is_array($resetToDefaults) && array_key_exists('reset_to_defaults', $resetToDefaults) && $resetToDefaults['reset_to_defaults'];
        if ($resetToDefaults) {
            foreach ($defaults as $groupId => $groupValues) {
                delete_option($groupId);
            }
        }

        // Transfer legacy options, if applicable.
        {
            $overwrites = array(
                'tps_general' => array(),
                'tps_nav' => array(),
                'tps_advanced' => array(),
                'tps_about' => array()
            );
            $refreshPageOnEachSlide = false;

            $options = get_option('tps_general');
            // New versions have "font" as the default button theme type. Older versions that have a classic theme chosen, will have the "classic" option chosen by default.
            if (is_array($options) && array_key_exists('theme', $options) && !array_key_exists('theme_type', $options)) {
                $overwrites['tps_general']['theme_type'] = 'classic';
            }

            $options = get_option('tps_nav');
            if (is_array($options)) {
	            if ( array_key_exists( 'refresh_page_on_slide', $options ) && $options['refresh_page_on_slide'] == true ) {
		            $refreshPageOnEachSlide = true;
	            }
	            if ( array_key_exists( 'enable_on_pages', $options ) && $options['enable_on_pages'] == true ) {
		            $overwrites['tps_advanced']['post_types'] = array('post', 'page');
	            }
            }

            $options = get_option('tps_advanced');
            if (is_array($options) && array_key_exists('slide_loading_mechanism', $options) && $options['slide_loading_mechanism'] == 'refresh') {
                $refreshPageOnEachSlide = true;
            }

            if ($refreshPageOnEachSlide) {
                $overwrites['tps_advanced']['slide_loading_mechanism'] = 'ajax';
                $overwrites['tps_advanced']['refresh_ads'] = true;
                $overwrites['tps_advanced']['ad_refreshing_mechanism'] = 'page';
            }
        }

	    // Transfer multiple selects.
	    $postTypes = get_option('tps_advanced_post_types');
	    if ($postTypes !== false) {
		    $overwrites['tps_advanced']['post_types'] = $postTypes;
		    delete_option( 'tps_advanced_post_types' );
	    }

	    // Sanitize, validate.
        foreach ($defaults as $groupId => $groupValues) {
            $options = get_option($groupId);

            if (!is_array($options)) {
                $options = array();
                $changed = true;
            } else {
                $changed = false;
            }

            // Add missing options.
            foreach ($groupValues as $key => $value) {
                if (isset($options[$key]) == false) {
                    $changed = true;
                    $options[$key] = $value;
                }
            }

            // Remove surplus options.
            foreach ($options as $key => $value) {
                if (isset($defaults[$groupId][$key]) == false) {
                    $changed = true;
                    unset($options[$key]);
                }
            }

            // Overwrite options.
            if (array_key_exists($groupId, $overwrites)) {
                foreach ($overwrites[$groupId] as $overwriteKey => $overwriteValue) {
                    $options[$overwriteKey] = $overwriteValue;
                    $changed = true;
                }
            }

            // Sanitize options.
            foreach ($options as $key => $value) {
                if (is_bool($defaults[$groupId][$key])) {
                    $options[$key] = ($options[$key] === true || $options[$key] === 'true') ? true : false;
                    $changed = true;
                }

                if (is_array($defaults[$groupId][$key])) {
                    $options[$key] = is_array($options[$key]) ? $options[$key] : $defaults[$groupId][$key];
                    $changed = true;
                }
            }

            // Validate options.
            if ($groupId == 'tps_general') {
                if (array_key_exists($options['transition_effect'], TpsOptions::getTransitionEffects()) == false) {
                    $options['transition_effect'] = $groupValues['transition_effect'];
                    $changed = true;
                }

                if ($options['transition_speed'] < 0) {
                    $options['transition_speed'] = $groupValues['transition_speed'];
                    $changed = true;
                }
            }

            if ($groupId == 'tps_nav') {
                if ($options['button_width'] < 0) {
                    $options['button_width'] = $groupValues['button_width'];
                    $changed = true;
                }
            }

            if ($groupId == 'tps_advanced') {
                if ($options['refresh_ads_every_n_slides'] < 1) {
                    $options['refresh_ads_every_n_slides'] = 1;
                    $changed = true;
                }
            }

            // Save options.
            if ($changed) {
                update_option($groupId, $options);
            }
        }
    }

    // Get post options
    public static function getPostOptions($postId)
    {
        $defaults = array(
            'enabled' => TpsOptions::get('default_activation_behavior', 'tps_advanced') == 0 ? false : true
        );

        $options = get_post_meta($postId, 'tps_options', true);
        if (!is_array($options)) {
            $options = array();
        }

        $options = array_merge($defaults, $options);

        return $options;
    }
}
