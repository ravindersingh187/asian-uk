<?php
/*
Plugin Name: Theia Post Slider
Description: Display multi-page posts using a slider, as a slideshow.
Author: WeCodePixels
Version: 1.6.4
*/

/*
 * Copyright 2012-2014, Theia Post Slider, WeCodePixels, http://wecodepixels.com
 */

/*
 * Plugin version. Used to forcefully invalidate CSS and JavaScript caches by appending the version number to the
 * filename (e.g. "style.css?ver=TPS_VERSION").
 */
define('TPS_VERSION', '1.6.4');

// Include other files.
include(dirname(__FILE__) . '/TpsMisc.php');
include(dirname(__FILE__) . '/TpsColors.php');
include(dirname(__FILE__) . '/TpsEnqueues.php');
include(dirname(__FILE__) . '/TpsShortCodes.php');
include(dirname(__FILE__) . '/TpsOptions.php');
include(dirname(__FILE__) . '/TpsAjax.php');
include(dirname(__FILE__) . '/TpsHelper.php');
include(dirname(__FILE__) . '/admin-menu.php');


// Add hooks.
add_action('init', 'TpsMisc::init');
add_action('init', 'TpsShortCodes::init');
add_action('get_the_excerpt', 'TpsMisc::beforeGetTheExcerpt', -999999);
add_action('get_the_excerpt', 'TpsMisc::afterGetTheExcerpt', 999999);
add_action('the_post', 'TpsMisc::the_post', 999999);
add_action('the_content', 'TpsMisc::the_content_early', 0);
add_action('the_content', 'TpsMisc::the_content', 999999);
add_action('wp_head', 'TpsMisc::echoInlineStyles');
add_action('wp_enqueue_scripts', 'TpsEnqueues::wp_enqueue_scripts');
add_action('admin_enqueue_scripts', 'TpsEnqueues::admin_enqueue_scripts');
add_filter('mce_buttons', 'TpsMisc::wysiwyg_editor');
add_action('init', 'TpsShortCodes::add_button');
add_filter('query_vars', 'TpsAjax::query_vars');
add_action('parse_request', 'TpsAjax::parse_request');
add_action('add_meta_boxes', 'TpsOptions::addMetaBoxes');
add_action('save_post', 'TpsOptions::savePost');
