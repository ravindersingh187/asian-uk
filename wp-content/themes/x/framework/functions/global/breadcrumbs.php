<?php

// =============================================================================
// FUNCTIONS/GLOBAL/BREADCRUMBS.PHP
// -----------------------------------------------------------------------------
// Sets up the breadcrumb navigation for the theme.
// =============================================================================

// =============================================================================
// TABLE OF CONTENTS
// -----------------------------------------------------------------------------
//   01. Breadcrumbs
// =============================================================================

// Breadcrumbs
// =============================================================================

if ( ! function_exists( 'x_breadcrumbs' ) ) :
  function x_breadcrumbs() {

    if ( get_theme_mod( 'x_breadcrumb_display' ) ) {

      //
      // 1. Delimiter between crumbs.
      // 2. Output text for the "Home" link.
      // 3. Link to the home page.
      // 4. Tag before the current crumb.
      // 5. Tag after the current crumb.
      // 6. Get page title.
      // 7. Get blog title.
      // 8. Get shop title.
      //

      GLOBAL $post;

      $stack          = x_get_stack();
      $delimiter      = ' <span class="delimiter"><i class="x-icon-angle-right"></i></span> '; // 1
      $home_text      = '<span class="home"><i class="x-icon-home"></i></span>';               // 2
      $home_link      = home_url();                                                            // 3
      $current_before = '<span class="current">';                                              // 4
      $current_after  = '</span>';                                                             // 5
      $page_title     = get_the_title();                                                       // 6
      $blog_title     = get_the_title( get_option( 'page_for_posts', true ) );                 // 7
      $shop_title     = get_theme_mod( 'x_' . $stack . '_shop_title' );                        // 8

      if ( function_exists( 'woocommerce_get_page_id' ) ) {
        $shop_url  = x_get_shop_link();
        $shop_link = '<a href="'. $shop_url .'">' . $shop_title . '</a>';
      }
     
      if ( is_front_page() ) {
        echo '<div class="x-breadcrumbs">' . $current_before . $home_text . $current_after . '</div>';
      } elseif ( is_home() ) {
        echo '<div class="x-breadcrumbs"><a href="' . $home_link . '">' . $home_text . '</a>' . $delimiter . $current_before . $blog_title . $current_after . '</div>';
      } else {
        echo '<div class="x-breadcrumbs"><a href="' . $home_link . '">' . $home_text . '</a>' . $delimiter;
        if ( is_category() ) {
          $the_cat = get_category( get_query_var( 'cat' ), false );
          if ( $the_cat->parent != 0 ) echo get_category_parents( $the_cat->parent, TRUE, $delimiter );
          echo $current_before . single_cat_title( '', false ) . $current_after;
        } elseif ( x_is_product_category() ) {
          echo $shop_link . $delimiter . $current_before . single_cat_title( '', false ) . $current_after;
        } elseif ( x_is_product_tag() ) {
          echo $shop_link . $delimiter . $current_before . single_tag_title( '', false ) . $current_after;
        } elseif ( is_search() ) {
          echo $current_before . __( 'Search Results for ', '__x__' ) . '&#8220;' . get_search_query() . '&#8221;' . $current_after;
        } elseif ( is_singular( 'post' ) ) {
          if ( get_option( 'page_for_posts' ) == is_front_page() ) {
            echo $current_before . $page_title . $current_after;
          } else {
            echo '<a href="' . get_permalink( get_option( 'page_for_posts' ) ) . '" title="' . esc_attr( __( 'See All Posts', '__x__' ) ) . '">' . $blog_title . '</a>' . $delimiter . $current_before . $page_title . $current_after;
          }
        } elseif ( x_is_portfolio() ) {
          echo $current_before . get_the_title() . $current_after;
        } elseif ( x_is_portfolio_item() ) {
          $link  = x_get_parent_portfolio_link();
          $title = x_get_parent_portfolio_title();
          echo '<a href="' . $link . '" title="' . esc_attr( __( 'See All Posts', '__x__' ) ) . '">' . $title . '</a>' . $delimiter . $current_before . $page_title . $current_after;
        } elseif ( x_is_product() ) {
          echo $shop_link . $delimiter . $current_before . $page_title . $current_after;
        } elseif ( is_page() && ! $post->post_parent ) {
          echo $current_before . $page_title . $current_after;
        } elseif ( is_page() && $post->post_parent ) {
          $parent_id   = $post->post_parent;
          $breadcrumbs = array();
          while ( $parent_id ) {
            $page          = get_page( $parent_id );
            $breadcrumbs[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
            $parent_id     = $page->post_parent;
          }
          $breadcrumbs = array_reverse( $breadcrumbs );
          for ( $i = 0; $i < count( $breadcrumbs ); $i++ ) {
            echo $breadcrumbs[$i];
            if ( $i != count( $breadcrumbs ) -1 ) echo $delimiter;
          }
          echo $delimiter . $current_before . $page_title . $current_after;
        } elseif ( is_tag() ) {
          echo $current_before . single_tag_title( '', false ) . $current_after;
        } elseif ( is_author() ) {
          GLOBAL $author;
          $userdata = get_userdata( $author );
          echo $current_before . __( 'Posts by ', '__x__' ) . '&#8220;' . $userdata->display_name . $current_after . '&#8221;';
        } elseif ( is_404() ) {
          echo $current_before . __( '404 (Page Not Found)', '__x__' ) . $current_after;
        } elseif ( is_archive() ) {
          if ( x_is_shop() ) {
            echo $current_before . $shop_title . $current_after;
          } else {
            echo $current_before . __( 'Archives ', '__x__' ) . $current_after;
          }
        }
        if ( get_query_var( 'paged' ) ) {
          echo ' <span class="current" style="white-space: nowrap;">(' . __( 'Page', '__x__' ) . ' ' . get_query_var( 'paged' ) . ')</span>';
        }
        echo '</div>';
      }

    }

  }
endif;