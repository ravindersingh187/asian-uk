<?php

// =============================================================================
// VIEWS/RENEW/_CONTENT.PHP
// -----------------------------------------------------------------------------
// Display of the_excerpt() or the_content() for various entries.
// =============================================================================

?>

<?php

if ( is_singular() || is_home() && get_theme_mod( 'x_blog_enable_full_post_content' ) == 1 ) :
  x_get_view( 'global', '_content', 'the-content' );
  x_get_view( 'renew', '_content', 'post-footer' );
else :
  x_get_view( 'global', '_content', 'the-excerpt' );
endif;

?>